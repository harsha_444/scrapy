import requests
import bs4
from bs4 import BeautifulSoup
import os
import time

p='0'
urls_list=[]
#For Downloading the existing files
#p='44769'
while(1): #Change it to while(1) during submission
    p=str(p)
    p=int(p)+1
    #print("list::",urls_list)
    url = 'http://judis.nic.in/supremecourt/imgst.aspx?filename='+str(p)
    #print("url",url)
    headers = {"User-Agent":"Mozilla/5.0"}
    response = requests.get(url, headers=headers)
    soup = bs4.BeautifulSoup(response.text, 'html.parser')
    #print(urls_list)
    all=soup.find("textarea",{"class":"StyleSheet1"}) #find, find_all
    try:
        x=all.text
    except AttributeError:
        print("All the available files are downloaded!!")
        break
    #print(x)
    p=str(p)
    filename='judgement'+str(p)+'.txt'
    if(os.path.exists(filename)):
        continue
    else:
        urls_list.append(int(p))
        amend=open(filename,'w')
        amend.write(x)
        print("Downloaded Judicial No: ",str(p))
        amend.close()

#For checking if the new files came
while(1):
    len_list=len(urls_list)
    print(urls_list)
    k=urls_list[len_list-1]+1
    print("Upcoming judicial: ",k)

    url = 'http://judis.nic.in/supremecourt/imgst.aspx?filename='+str(k)
    #print("url",url)
    headers = {"User-Agent":"Mozilla/5.0"}
    response = requests.get(url, headers=headers)
    soup = bs4.BeautifulSoup(response.text, 'html.parser')
    all=soup.find("textarea",{"class":"StyleSheet1"}) #find, find_all
    try:
        x=all.text
    except AttributeError:
        print("No new File added..!! Please check back later")
        time.sleep(10)
        continue
    #print(x)
    print("New file added, and is being downloaded.....")
    k=str(k)
    filename='judgement'+str(k)+'.txt'


    if(os.path.exists(filename)):
        continue
    else:
        amend=open(filename,'w')
        amend.write(x)
        print("Downloaded Judicial No: ",str(k))
        amend.close()
        k=int(k)
        urls_list.append(k)
        print(urls_list)
    #break
